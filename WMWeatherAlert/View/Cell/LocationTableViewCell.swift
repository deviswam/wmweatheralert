//
//  LocationTableViewCell.swift
//  WMWeatherAlert
//
//  Created by Waheed Malik on 18/03/2017.
//  Copyright © 2017 Waheed Malik. All rights reserved.
//

import UIKit

protocol LocationTableViewCell {
    func configCell(withLocation location: LocationViewModel)
}

class WMLocationTableViewCell: UITableViewCell, LocationTableViewCell {
    
    @IBOutlet weak var locationNameLabel: UILabel!
    @IBOutlet weak var windSpeedLabel: UILabel!
    @IBOutlet weak var windDirectionLabel: UILabel!
    
    func configCell(withLocation location: LocationViewModel) {
        locationNameLabel.text = location.name
        windSpeedLabel.text = location.currentWindSpeed
        windDirectionLabel.text = location.currentWindDirection
    }
}
