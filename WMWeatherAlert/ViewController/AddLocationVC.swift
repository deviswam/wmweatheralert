//
//  AddLocationVC.swift
//  WMWeatherAlert
//
//  Created by Waheed Malik on 18/03/2017.
//  Copyright © 2017 Waheed Malik. All rights reserved.
//

import UIKit

class WMAddLocationVC: UIViewController {
    @IBOutlet weak var locationSearchBar: UISearchBar!
    @IBOutlet weak var locationsTableView: UITableView!
    
    var addLocationTableViewDataSource: UITableViewDataSource?
    var addLocationTableViewDelegate: UITableViewDelegate?
    var locationSearchBarDelegate: UISearchBarDelegate?
    var viewModel: AddLocationViewModel?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        locationsTableView.dataSource = addLocationTableViewDataSource
        locationsTableView.delegate = addLocationTableViewDelegate
        locationSearchBar.delegate = locationSearchBarDelegate
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        locationSearchBar.becomeFirstResponder()
    }
}

extension WMAddLocationVC: AddLocationViewModelViewDelegate {
    func locationsLoaded(viewModel: AddLocationViewModel) {
        locationsTableView.reloadData()
    }
}
