//
//  FavLocationsVC.swift
//  WMWeatherAlert
//
//  Created by Waheed Malik on 18/03/2017.
//  Copyright © 2017 Waheed Malik. All rights reserved.
//

import UIKit

class WMFavLocationsVC: UIViewController {
    
    @IBOutlet weak var locationsTableView: UITableView!
    var locationsTableViewDataSource: UITableViewDataSource?
    var locationsTableViewDelegate: UITableViewDelegate?
    var viewModel: FavLocationsViewModel?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        locationsTableView.dataSource = locationsTableViewDataSource
        locationsTableView.delegate = locationsTableViewDelegate
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        loadFavLocations()
    }
    
    func loadFavLocations() {
        guard let viewModel = viewModel else {
            return
        }
        viewModel.loadFavouriteLocations()
    }
    
    @IBAction func addLocationButtonPressed(_ sender: Any) {
        viewModel?.showAddFavouriteLocation()
    }
}

extension WMFavLocationsVC: FavLocationsViewModelViewDelegate {
    func locationsLoaded(viewModel: FavLocationsViewModel) {
        locationsTableView.reloadData()
    }
}
