//
//  AddLocationsTableViewDataSource.swift
//  WMWeatherAlert
//
//  Created by Waheed Malik on 19/03/2017.
//  Copyright © 2017 Waheed Malik. All rights reserved.
//

import UIKit

class WMAddLocationTableViewDataSource: NSObject, UITableViewDataSource {
    var viewModel: AddLocationViewModel?
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if let viewModel = viewModel {
            return viewModel.numberOfSearchedLocations()
        }
        return 0
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "LocationTableViewCell", for: indexPath)
        if let location = viewModel?.location(at: indexPath.row), let textLabel = cell.textLabel {
            textLabel.text = location.name
        }
        return cell
    }
}
