//
//  LocationsTableViewDataSource.swift
//  WMWeatherAlert
//
//  Created by Waheed Malik on 19/03/2017.
//  Copyright © 2017 Waheed Malik. All rights reserved.
//

import UIKit

class WMLocationsTableViewDataSource: NSObject, UITableViewDataSource {
    var viewModel: FavLocationsViewModel?
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if let viewModel = viewModel {
            return viewModel.numberOfFavouriteLocations()
        }
        return 0
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "LocationTableViewCell", for: indexPath)
        if let locationCell = cell as? LocationTableViewCell,
            let location = viewModel?.location(at: indexPath.row) {
            locationCell.configCell(withLocation: location)
        }
        return cell
    }
}
