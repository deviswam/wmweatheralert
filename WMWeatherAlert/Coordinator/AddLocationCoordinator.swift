//
//  WMSearchLocationCoordinator.swift
//  WMWeatherAlert
//
//  Created by Waheed Malik on 18/03/2017.
//  Copyright © 2017 Waheed Malik. All rights reserved.
//

import UIKit

class WMAddLocationCoordinator {
    fileprivate var navigationController: UINavigationController
    init(navigationController: UINavigationController) {
        self.navigationController = navigationController
    }
    
    func start() {
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        if let addLocationVC = storyboard.instantiateViewController(withIdentifier: "WMAddLocationVC") as? WMAddLocationVC {
            let weatherManager = SharedComponents.weatherManager
            let viewModel = WMAddLocationViewModel(weatherManager: weatherManager, viewDelegate: addLocationVC, coordinatorDelegate: self)
            let addLocationTableViewDelegate = WMAddLocationTableViewDelegate()
            let addLocationTableViewDataSource = WMAddLocationTableViewDataSource()
            let addLocationSearchBarDelegate = WMAddLocationSearchBarDelegate()
            addLocationVC.viewModel = viewModel
            addLocationSearchBarDelegate.viewModel = viewModel
            addLocationTableViewDataSource.viewModel = viewModel
            addLocationTableViewDelegate.viewModel = viewModel
            addLocationVC.addLocationTableViewDelegate = addLocationTableViewDelegate
            addLocationVC.addLocationTableViewDataSource = addLocationTableViewDataSource
            addLocationVC.locationSearchBarDelegate = addLocationSearchBarDelegate
            self.navigationController.pushViewController(addLocationVC, animated: true)
        }
    }
}

extension WMAddLocationCoordinator: AddLocationViewModelCoordinatorDelegate {
    func didAddFavouriteLocation(viewModel: AddLocationViewModel) {
        self.navigationController.popViewController(animated: true)
    }
}
