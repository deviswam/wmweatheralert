//
//  WMFavLocationsCoordinator.swift
//  WMWeatherAlert
//
//  Created by Waheed Malik on 18/03/2017.
//  Copyright © 2017 Waheed Malik. All rights reserved.
//

import UIKit

class WMFavLocationsCoordinator {
    private var window: UIWindow
    fileprivate var rootNavController: UINavigationController?
    init(window: UIWindow) {
        self.window = window
    }
    
    func start() {
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        if let rootNavController = storyboard.instantiateInitialViewController() as? UINavigationController {
            self.rootNavController = rootNavController
            self.window.rootViewController = rootNavController
            self.showFavLocations(rootNavController: rootNavController)
        }
    }
    
    private func showFavLocations(rootNavController: UINavigationController) {
        if let favLocationsVC = rootNavController.topViewController as? WMFavLocationsVC {
            let weatherManager = SharedComponents.weatherManager
            let locationsTableViewDataSource = WMLocationsTableViewDataSource()
            let viewModel = WMFavLocationsViewModel(weatherManager: weatherManager, viewDelegate: favLocationsVC, coordinatorDelegate: self)
            favLocationsVC.viewModel = viewModel
            favLocationsVC.locationsTableViewDataSource = locationsTableViewDataSource
            locationsTableViewDataSource.viewModel = viewModel
        }
    }
}

extension WMFavLocationsCoordinator: FavLocationsViewModelCoordinatorDelegate {
    func navigateToAddFavouriteLocation() {
        guard let rootNavContoller = self.rootNavController else { return }
        let addLocationCoordinator = WMAddLocationCoordinator(navigationController: rootNavContoller)
        addLocationCoordinator.start()
    }
}
