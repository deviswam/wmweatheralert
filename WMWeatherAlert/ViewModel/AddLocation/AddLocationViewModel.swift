//
//  AddLocationViewModel.swift
//  WMWeatherAlert
//
//  Created by Waheed Malik on 18/03/2017.
//  Copyright © 2017 Waheed Malik. All rights reserved.
//

import Foundation

protocol AddLocationViewModelViewDelegate {
    func locationsLoaded(viewModel: AddLocationViewModel)
}

protocol AddLocationViewModelCoordinatorDelegate {
    func didAddFavouriteLocation(viewModel: AddLocationViewModel)
}

protocol AddLocationViewModel {
    // getter accessors
    func numberOfSearchedLocations() -> Int
    func location(at index: Int) -> LocationViewModel?
    
    // callers
    func searchLocation(with name: String)
    func selectedLocation(at index: Int)
}

class WMAddLocationViewModel: AddLocationViewModel {
    private var weatherManager: WeatherManager
    private var viewDelegate: AddLocationViewModelViewDelegate
    private var coordinatorDelegate: AddLocationViewModelCoordinatorDelegate
    private var locations: [Location]? {
        didSet {
            self.viewDelegate.locationsLoaded(viewModel: self)
        }
    }

    init(weatherManager: WeatherManager, viewDelegate: AddLocationViewModelViewDelegate, coordinatorDelegate: AddLocationViewModelCoordinatorDelegate) {
        self.weatherManager = weatherManager
        self.viewDelegate = viewDelegate
        self.coordinatorDelegate = coordinatorDelegate
    }
    
    func searchLocation(with name: String) {
        weatherManager.searchLocation(with: name) { (locations: [Location]?, error: WeatherManagerError?) in
            self.locations = locations
        }
    }
    
    func numberOfSearchedLocations() -> Int {
        if let locations = self.locations { return locations.count }
        return 0
    }
    
    func location(at index: Int) -> LocationViewModel? {
        if let locations = locations, index < locations.count {
            return WMLocationViewModel(location: locations[index])
        }
        return nil
    }
    
    func selectedLocation(at index: Int) {
        guard let locations = self.locations, index < locations.count else { return }
        let selectedLocation = locations[index]
        let isLocationAdded = weatherManager.addFavouriteLocation(location: selectedLocation)
        if isLocationAdded {
            //navigate back to previous view by telling coordinator that fav location is added successfully.
            self.coordinatorDelegate.didAddFavouriteLocation(viewModel: self)
        } else {
            print("Info: Unable to add favourite Location.")
        }
    }
}
