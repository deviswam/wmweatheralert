//
//  LocationViewModel.swift
//  WMWeatherAlert
//
//  Created by Waheed Malik on 18/03/2017.
//  Copyright © 2017 Waheed Malik. All rights reserved.
//

import Foundation

protocol LocationViewModel {
    var name: String { get }
    var currentWindSpeed: String { get }
    var currentWindDirection: String { get }
}

class WMLocationViewModel: LocationViewModel {
    var name: String = ""
    var currentWindSpeed: String = ""
    var currentWindDirection: String = ""
    init(location: Location) {
        if let name = location.name {
            self.name = name
            if let country = location.country {
                self.name += ", \(country)"
            }
        }
        
        if let speed = location.currentWindCondition?.speed {
            currentWindSpeed = "\(speed) mph"
        }
        
        if let direction = location.currentWindCondition?.direction {
            currentWindDirection = direction
        }
    }
}

