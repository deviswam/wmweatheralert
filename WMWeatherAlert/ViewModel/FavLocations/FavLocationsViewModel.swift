//
//  WMFavLocationsViewModel.swift
//  WMWeatherAlert
//
//  Created by Waheed Malik on 18/01/2017.
//  Copyright © 2017 Waheed Malik. All rights reserved.
//

import Foundation

protocol FavLocationsViewModelViewDelegate {
    func locationsLoaded(viewModel: FavLocationsViewModel)
}

protocol FavLocationsViewModelCoordinatorDelegate {
    func navigateToAddFavouriteLocation()
}

protocol FavLocationsViewModel: class {
    // getter accessors
    func numberOfFavouriteLocations() -> Int
    func location(at index: Int) -> LocationViewModel?
    
    // callers
    func loadFavouriteLocations()
    func showAddFavouriteLocation()
}

class WMFavLocationsViewModel: FavLocationsViewModel {
    private var weatherManager: WeatherManager
    private var viewDelegate: FavLocationsViewModelViewDelegate
    private var coordinatorDelegate: FavLocationsViewModelCoordinatorDelegate
    private var locations: [Location]? {
        didSet {
            self.viewDelegate.locationsLoaded(viewModel: self)
        }
    }

    init(weatherManager: WeatherManager, viewDelegate: FavLocationsViewModelViewDelegate, coordinatorDelegate: FavLocationsViewModelCoordinatorDelegate) {
        self.weatherManager = weatherManager
        self.viewDelegate = viewDelegate
        self.coordinatorDelegate = coordinatorDelegate
    }
    
    func loadFavouriteLocations() {
        weatherManager.favLocations { (locations: [Location]?, error: WeatherManagerError?) in
            self.locations = locations
        }
    }
    
    func numberOfFavouriteLocations() -> Int {
        if let locations = self.locations { return locations.count }
        return 0
    }
    
    func location(at index: Int) -> LocationViewModel? {
        if let locations = locations, index < locations.count {
            return WMLocationViewModel(location: locations[index])
        }
        return nil
    }
    
    func showAddFavouriteLocation() {
        coordinatorDelegate.navigateToAddFavouriteLocation()
    }
}
