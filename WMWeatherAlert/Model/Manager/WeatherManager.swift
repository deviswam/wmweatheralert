//
//  WeatherManager.swift
//  WMWeatherAlert
//
//  Created by Waheed Malik on 18/03/2017.
//  Copyright © 2017 Waheed Malik. All rights reserved.
//

import Foundation

enum WeatherManagerError : Error {
    case noLocationsFoundError
    case unableToStoreLocationAsFavourite
    
    func userFriendlyMessage() -> String {
        switch self {
        case .noLocationsFoundError:
            return "No Locations Found"
        case .unableToStoreLocationAsFavourite:
            return "Unable to store location as Favourite"
        }
    }
}

protocol WeatherManager {
    func favLocations(completionHandler: @escaping (_ locations: [Location]?, _ error: WeatherManagerError?) -> Void)
    func searchLocation(with name: String, completionHandler: @escaping (_ locations: [Location]?, _ error: WeatherManagerError?) -> Void)
    func addFavouriteLocation(location: Location) -> Bool
}

class WMWeatherManager: WeatherManager {
    private let userDefaultsManager: UserDefaultsManager
    private let weatherAPI: WeatherAPI
    init(userDefaultsManager: UserDefaultsManager, weatherAPI: WeatherAPI) {
        self.userDefaultsManager = userDefaultsManager
        self.weatherAPI = weatherAPI
    }
    
    func favLocations(completionHandler: @escaping (_ locations: [Location]?, _ error: WeatherManagerError?) -> Void) {
        self.userDefaultsManager.favLocationIds { (locationIds: [Int]?) in
            if let locationIds = locationIds {
                self.weatherAPI.currentWindCondition(for: locationIds, completionHandler: { (locations: [Location]?, error: WeatherAPIError?) in
                    completionHandler(locations, nil)
                })
            } else {
                completionHandler(nil, WeatherManagerError.noLocationsFoundError)
            }
        }
    }
    
    func searchLocation(with name: String, completionHandler: @escaping ([Location]?, WeatherManagerError?) -> Void) {
        self.weatherAPI.findCities(with: name) { (locations: [Location]?, error: WeatherAPIError?) in
            completionHandler(locations,nil)
        }
    }
    
    func addFavouriteLocation(location: Location) -> Bool {
        return self.userDefaultsManager.addFavLocation(id: location.id)
    }
}
