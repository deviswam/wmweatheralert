//
//  WMUserDefaultsManager.swift
//  WMWeatherAlert
//
//  Created by Waheed Malik on 18/03/2017.
//  Copyright © 2017 Waheed Malik. All rights reserved.
//

import Foundation

protocol UserDefaultsManager {
    func favLocationIds(completionHandler: @escaping ([Int]?) -> Void)
    func addFavLocation(id: Int) -> Bool
}

class WMUserDefaultsManager: UserDefaultsManager {
    let kFAV_LOCATION_IDS = "fav_locations"
    
    private let userDefaults: UserDefaults
    init(userDefaults: UserDefaults) {
        self.userDefaults = userDefaults
    }
    
    func favLocationIds(completionHandler: @escaping ([Int]?) -> Void) {
        completionHandler(userDefaults.array(forKey: kFAV_LOCATION_IDS) as? [Int])
    }
    
    func addFavLocation(id: Int) -> Bool {
        if userDefaults.array(forKey: kFAV_LOCATION_IDS) == nil {
            userDefaults.setValue([Int](), forKey: kFAV_LOCATION_IDS)
        }
        
        if var ids = userDefaults.array(forKey: kFAV_LOCATION_IDS) as? [Int] {
            if ids.contains(id) == false {
                ids.append(id)
                userDefaults.setValue(ids, forKey: kFAV_LOCATION_IDS)
                return userDefaults.synchronize()
            }
        }
        return false
    }
}
