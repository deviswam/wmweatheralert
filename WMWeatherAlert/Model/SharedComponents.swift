//
//  SharedComponents.swift
//  WMWeatherAlert
//
//  Created by Waheed Malik on 19/03/2017.
//  Copyright © 2017 Waheed Malik. All rights reserved.
//

import Foundation

class SharedComponents {
    static let weatherManager: WeatherManager = {
        let weatherManager = WMWeatherManager(userDefaultsManager: SharedComponents.userDefaultsManager, weatherAPI: SharedComponents.weatherAPI)
        return weatherManager
    }()
    
    static let userDefaultsManager: UserDefaultsManager = {
        let userDefaultsManager = WMUserDefaultsManager(userDefaults: UserDefaults.standard)
        return userDefaultsManager
    }()
    
    static let weatherAPI: WeatherAPI = {
        let weatherAPI = WMWeatherAPI()
        return weatherAPI
    }()
}
