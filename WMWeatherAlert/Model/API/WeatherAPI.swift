//
//  WeatherAPI.swift
//  WMWeatherAlert
//
//  Created by Waheed Malik on 18/03/2017.
//  Copyright © 2017 Waheed Malik. All rights reserved.
//

import Foundation

enum WeatherAPIError: Error {
    case httpError
    case invalidDataError
    case dataSerializationError
}

protocol WeatherAPI {
    func currentWindCondition(for locationIds: [Int], completionHandler: @escaping (_ locations: [Location]?, _ error: WeatherAPIError?) -> Void)
    func findCities(with matchingName: String, completionHandler: @escaping (_ locations: [Location]?, _ error: WeatherAPIError?) -> Void)
}

enum WeatherAPIMethodType: String {
    case currentWeatherForMultipleLocations = "group"
    case findCities = "find"
}

class WMWeatherAPI: WeatherAPI {
    
    let API_KEY = "4b735ecc95c764e35c9842d5efb5de55"
    let BASE_URL = "http://api.openweathermap.org/data/2.5/"
    
    private let urlSession: URLSession!
    init(urlSession: URLSession = URLSession.shared) {
        self.urlSession = urlSession
    }
    
    func findCities(with matchingName: String, completionHandler: @escaping ([Location]?, WeatherAPIError?) -> Void) {
        let requestParams = ["q": matchingName.trimmingCharacters(in: .whitespacesAndNewlines),
                             "type": "accurate",
                             "appid":API_KEY]
        guard let urlString = self.createURLString(with: .findCities, parameters: requestParams),
            let url = URL(string: urlString) else {
                return
        }
        print("WAM URL:\(urlString)")
        getLocations(with: url) { (locations: [Location]?, error: WeatherAPIError?) in
            completionHandler(locations, error)
        }
    }
    
    func currentWindCondition(for locationIds: [Int], completionHandler: @escaping (_ locations: [Location]?, _ error: WeatherAPIError?) -> Void) {
        
        let locationIdsString = locationIds.map{"\($0)"}.joined(separator: ",")
        let requestParams = ["id": locationIdsString,
                             "units": "imperial",
                             "appid":API_KEY]
        guard let urlString = self.createURLString(with: .currentWeatherForMultipleLocations, parameters: requestParams),
              let url = URL(string: urlString) else {
            return
        }
        print("WAM URL:\(urlString)")
        getLocations(with: url) { (locations: [Location]?, error: WeatherAPIError?) in
            completionHandler(locations, error)
        }
    }
    
    
    private func getLocations(with url: URL, completionHandler: @escaping ([Location]?, WeatherAPIError?) -> Void) {
        let request = URLRequest(url: url, cachePolicy: URLRequest.CachePolicy.reloadIgnoringLocalAndRemoteCacheData, timeoutInterval: 10)
        let dataTask = urlSession.dataTask(with: request) { (data, urlResponse, error) in
            var locations : [Location]?
            var retError : WeatherAPIError?
            
            if error != nil {
                retError = WeatherAPIError.httpError
            } else if let data = data {
                do {
                    if let dictionary = try JSONSerialization.jsonObject(with: data, options: []) as? [String : AnyObject] {
                        guard let jsonLocationsCurrentWeather = dictionary["list"] as? [[String : AnyObject]] else {
                            retError = WeatherAPIError.invalidDataError
                            DispatchQueue.main.async {
                                completionHandler(locations, retError)
                            }
                            return
                        }
                        locations = self.getLocationsPopulatedWithCurrentWeather(from: jsonLocationsCurrentWeather)
                    }
                } catch {
                    retError = WeatherAPIError.dataSerializationError
                }
            }
            
            DispatchQueue.main.async {
                completionHandler(locations, retError)
            }
        }
        dataTask.resume()
    }
    
    private func createURLString(with apiMethodType:WeatherAPIMethodType, parameters:[String: String]) -> String? {
        var urlComponents = URLComponents(string: BASE_URL + apiMethodType.rawValue)
        var queryItems = [URLQueryItem]()
        for (key, value) in parameters {
            let query = URLQueryItem(name: key, value: value)
            queryItems.append(query)
        }
        urlComponents?.queryItems = queryItems
        return urlComponents?.url?.absoluteString
    }
    
    private func getLocationsPopulatedWithCurrentWeather(from jsonLocations:[[String : AnyObject]]) -> [Location] {
        var locations = [Location]()
        
        for jsonLocation in jsonLocations {
            if let location = WMLocation(dictionary: jsonLocation) {
                locations.append(location)
            }
        }
        return locations
    }
}
