//
//  Utility.swift
//  WMWeatherAlert
//
//  Created by Waheed Malik on 18/03/2017.
//  Copyright © 2017 Waheed Malik. All rights reserved.
//

import Foundation

class Utility {
    static func degreeToCompass(degree: Float) -> String {
        let val = Int((degree/22.5) + 0.5)
        let arr = ["N","NNE","NE","ENE","E","ESE", "SE", "SSE","S","SSW","SW","WSW","W","WNW","NW","NNW"]
        print("\(arr[(val % 16)])")
        return arr[(val % 16)]
    }
}
