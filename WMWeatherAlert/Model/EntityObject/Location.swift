//
//  WMLocation.swift
//  WMWeatherAlert
//
//  Created by Waheed Malik on 18/03/2017.
//  Copyright © 2017 Waheed Malik. All rights reserved.
//

import Foundation

protocol Location {
    var id: Int { get }
    var name: String? { get }
    var country: String? { get }
    var currentWindCondition: WindCondition? { get }
}

struct WMLocation: Location, ObjectMapper {
    var id: Int
    var name: String?
    var country: String?
    var currentWindCondition: WindCondition?
    init(id: Int, name: String? = nil, country: String? = nil, currentWindCondition: WindCondition? = nil) {
        self.id = id
        self.name = name
        self.country = country
        self.currentWindCondition = currentWindCondition
    }
    
    init?(dictionary: Dictionary<String, Any>?) {
        guard let dictionary = dictionary,
              let dId = dictionary["id"] as? Int else {
            return nil
        }
        
        let dName = dictionary["name"] as? String
        
        var dCountry: String?
        if let countryDictionary = dictionary["sys"] as? Dictionary<String, Any> {
            dCountry = countryDictionary["country"] as? String
        }
        
        var dCurrentWindCondition: WindCondition?
        if var currentWindConditionDictionary = dictionary["wind"] as? Dictionary<String, Any> {
            if let dateUnix = dictionary["dt"] {
                currentWindConditionDictionary["dt"] = dateUnix
                dCurrentWindCondition = WMWindCondition(dictionary: currentWindConditionDictionary)
            }
        }
        self.init(id: dId, name:dName, country: dCountry, currentWindCondition: dCurrentWindCondition)
    }
}

func == (lhs: Location, rhs: Location) -> Bool {
    if lhs.id == rhs.id {
        return true
    }
    return false
}
