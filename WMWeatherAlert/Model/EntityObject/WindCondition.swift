//
//  WindCondition.swift
//  WMWeatherAlert
//
//  Created by Waheed Malik on 18/03/2017.
//  Copyright © 2017 Waheed Malik. All rights reserved.
//

import Foundation

protocol WindCondition {
    var speed: Double? { get } // in mph
    var direction: String? { get } // cardinal direction i.e: SSE
    var date: Date? { get }
}

struct WMWindCondition: WindCondition, ObjectMapper {
    var speed: Double?
    var direction: String?
    var date: Date?
    
    init(speed: Double? = nil, direction: String? = nil, date: Date? = nil) {
        self.speed = speed
        self.direction = direction
        self.date = date
    }
    
    init?(dictionary: Dictionary<String, Any>?) {
        guard let dictionary = dictionary else {
            return nil
        }
        
        let speedInMph = dictionary["speed"] as? Double
        var direction: String?
        if let directionInDegrees = dictionary["deg"] as? TimeInterval {
            direction = Utility.degreeToCompass(degree: Float(directionInDegrees))
        }
        var date: Date?
        if let dateUnix = dictionary["dt"] as? Double {
            date = Date(timeIntervalSince1970: dateUnix)
        }
        self.init(speed: speedInMph, direction: direction, date: date)
    }
}
