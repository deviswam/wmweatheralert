//
//  AddLocationTableViewDelegate.swift
//  WMWeatherAlert
//
//  Created by Waheed Malik on 19/03/2017.
//  Copyright © 2017 Waheed Malik. All rights reserved.
//

import UIKit

class WMAddLocationTableViewDelegate: NSObject, UITableViewDelegate {
    var viewModel: AddLocationViewModel?
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        viewModel?.selectedLocation(at: indexPath.row)
    }
}
