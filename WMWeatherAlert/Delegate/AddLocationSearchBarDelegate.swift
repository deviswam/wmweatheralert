//
//  AddLocationSearchBarDelegate.swift
//  WMWeatherAlert
//
//  Created by Waheed Malik on 18/03/2017.
//  Copyright © 2017 Waheed Malik. All rights reserved.
//

import UIKit

class WMAddLocationSearchBarDelegate: NSObject, UISearchBarDelegate {
    var viewModel: AddLocationViewModel?
    func searchBarCancelButtonClicked(_ searchBar: UISearchBar) {
        print("searchBarCancelButtonClicked")
    }
    
    func searchBarSearchButtonClicked(_ searchBar: UISearchBar) {
        if let viewModel = viewModel, let text = searchBar.text {
            viewModel.searchLocation(with: text)
        }
    }
}
