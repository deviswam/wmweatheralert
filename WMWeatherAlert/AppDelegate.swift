//
//  AppDelegate.swift
//  WMWeatherAlert
//
//  Created by Waheed Malik on 18/03/2017.
//  Copyright © 2017 Waheed Malik. All rights reserved.
//

import UIKit

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate {

    var window: UIWindow?
    var favLocationsCoordinator: WMFavLocationsCoordinator!
    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplicationLaunchOptionsKey: Any]?) -> Bool {
        window = UIWindow()
        favLocationsCoordinator = WMFavLocationsCoordinator(window: window!)
        favLocationsCoordinator.start()
        window?.makeKeyAndVisible()
        return true
    }
}

