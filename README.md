PLEASE READ ALL
===============

1. iOS 8 onwards supported. Tested on iPhone 5 onwards devices in both orientation.
2. Because of time constraints only Fav Locations(Home screen) and Add Location screens are completed. I wish i would have more time to complete the Forecase detail view. 
3. The app architecture is already in place with TDD patterns and practices adopted on above mentioned two screens and can be used in pretty much the same way on Forecase details view if time permits.
4. The app is following MVVM-C (Model-View-ViewModel-Coordinator) architecture pattern.
5. Because of limited time, some test cases are not covered for existing components i.e: Test cases for Error handling on WeatherManager and WeatherAPI layers as the focus was to complete the happy path first. Despite of that application code is stable and doesn't crash on httpError and serialization scenarios for example.
6. Please check the xcode console for user info and when the app makes web request. The alert view and activity indicator are not present to engage with user about such activity. Also because of limited time.
7. Because of limited time, test cases are not refactored to follow DRY principle hence there would be some repetition in few test cases.
8. There are no functional tests written because of limited time. I wish i could have the chance to write them using KIF or iOS UITesting. I've written them in past.
9. Whereever possible i've tried to follow SOLID principles, composition over inheritance and protocol oriented programming.
10. Please turn on the Software keyboard in the simulator to see the iOS keyboard on Add Location View.

I've also attached another technical test(ClothsCatalog) that i've done sometime back for a company. It is a complete test with all areas covered and could give an idea to the reviewer about any missing things mentioned above. This project has its own Readme.md file.

Thanks for reading.
