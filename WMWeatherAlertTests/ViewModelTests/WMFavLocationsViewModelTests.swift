//
//  FavLocationsViewModelTests.swift
//  WMWeatherAlert
//
//  Created by Waheed Malik on 18/03/2017.
//  Copyright © 2017 Waheed Malik. All rights reserved.
//

import XCTest
@testable import WMWeatherAlert

extension WMFavLocationsViewModelTests {
    class MockWeatherManager: WeatherManager {
        var getSavedLocationsCalled = false
        var completionHandler: (([Location]?, WeatherManagerError?) -> Void)?
        
        func favLocations(completionHandler: @escaping (_ locations: [Location]?, _ error: WeatherManagerError?) -> Void) {
            self.getSavedLocationsCalled = true
            self.completionHandler = completionHandler
        }
        
        func searchLocation(with name: String, completionHandler: @escaping ([Location]?, WeatherManagerError?) -> Void) {
        }
        
        func addFavouriteLocation(location: Location) -> Bool {
            return false
        }
    }
    
    class MockFavLocationsViewModelViewDelegate: FavLocationsViewModelViewDelegate {
        var expectation: XCTestExpectation?
        var locationsLoadedDelegateMethodCalled = false
        
        func locationsLoaded(viewModel: FavLocationsViewModel) {
            guard let asynExpectation = expectation else {
                XCTFail("MockFavLocationsViewModelDelegate was not setup correctly. Missing XCTExpectation reference")
                return
            }
            locationsLoadedDelegateMethodCalled = true
            asynExpectation.fulfill()
        }
    }
    
    class MockFavLocationsViewModelCoordinatorDelegate: FavLocationsViewModelCoordinatorDelegate {
        func navigateToAddFavouriteLocation() {
            
        }
    }
}

class WMFavLocationsViewModelTests: XCTestCase {
    
    var sut: WMFavLocationsViewModel!
    var mockWeatherManager: MockWeatherManager!
    var mockViewDelegate: MockFavLocationsViewModelViewDelegate!
    var mockCoordinatorDelegate: MockFavLocationsViewModelCoordinatorDelegate!
    
    override func setUp() {
        super.setUp()
        
        mockWeatherManager = MockWeatherManager()
        mockViewDelegate = MockFavLocationsViewModelViewDelegate()
        mockCoordinatorDelegate = MockFavLocationsViewModelCoordinatorDelegate()
        sut = WMFavLocationsViewModel(weatherManager: mockWeatherManager, viewDelegate: mockViewDelegate, coordinatorDelegate: mockCoordinatorDelegate)
    }
    
    func testLoadFavLocations_shouldAskWeatherManagerForSavedLocations() {
        // Act
        sut.loadFavouriteLocations()
        // Assert
        XCTAssert(mockWeatherManager.getSavedLocationsCalled)
    }
    
    
    func testLoadFavLocations_whenNoLocationsFound_raisesLocationsLoadedDelegateCallback() {
        // Arrange
        let asynExpectation = expectation(description: "asyncFunction")
        mockViewDelegate.expectation = asynExpectation
        
        // Act
        sut.loadFavouriteLocations()
        mockWeatherManager.completionHandler?(nil, nil)
        
        waitForExpectations(timeout: 1.0) {[unowned self] error in
            if let error = error {
                XCTFail("waitForExpectationsWithTimeout errored: \(error)")
            }
            XCTAssert(self.mockViewDelegate.locationsLoadedDelegateMethodCalled)
        }
    }

    func testNoOfLocations_whenNoLocationsFound_returnsZero() {
        // Arrange
        let asynExpectation = expectation(description: "asyncFunction")
        mockViewDelegate.expectation = asynExpectation
        
        // Act
        sut.loadFavouriteLocations()
        mockWeatherManager.completionHandler?(nil, nil)
        
        // Assert
        waitForExpectations(timeout: 1.0) { error in
            if let error = error {
                XCTFail("waitForExpectationsWithTimeout errored: \(error)")
            }
            XCTAssertEqual(self.sut.numberOfFavouriteLocations(), 0)
        }
    }
    
    func testNoOfLocations_whenOneLocationFound_returnsOne() {
        // Arrange
        let asynExpectation = expectation(description: "asyncFunction")
        mockViewDelegate.expectation = asynExpectation
        
        // Act
        sut.loadFavouriteLocations()
        mockWeatherManager.completionHandler?([WMLocation(id: 123)], nil)
        
        // Assert
        waitForExpectations(timeout: 1.0) { error in
            if let error = error {
                XCTFail("waitForExpectationsWithTimeout errored: \(error)")
            }
            XCTAssertEqual(self.sut.numberOfFavouriteLocations(), 1)
        }
    }
    
    func testNoOfLocations_whenFiveLocationsFound_returnsFive() {
        // Arrange
        let asynExpectation = expectation(description: "asyncFunction")
        mockViewDelegate.expectation = asynExpectation
        
        // Act
        sut.loadFavouriteLocations()
        mockWeatherManager.completionHandler?([WMLocation(id: 123), WMLocation(id: 456), WMLocation(id: 789), WMLocation(id: 567), WMLocation(id: 876)], nil)
        
        // Assert
        waitForExpectations(timeout: 1.0) { error in
            if let error = error {
                XCTFail("waitForExpectationsWithTimeout errored: \(error)")
            }
            XCTAssertEqual(self.sut.numberOfFavouriteLocations(), 5)
        }
    }
    
    func testlocationAtIndex_returnsTheCorrectLocationViewModelObject() {
        // Arrange
        let asynExpectation = expectation(description: "asyncFunction")
        mockViewDelegate.expectation = asynExpectation
        
        // Act
        sut.loadFavouriteLocations()
        mockWeatherManager.completionHandler?([WMLocation(id: 123, name: "London"), WMLocation(id: 456, name: "HongKong")], nil)
        
        // Assert
        waitForExpectations(timeout: 1.0) { error in
            if let error = error {
                XCTFail("waitForExpectationsWithTimeout errored: \(error)")
            }
            let london = self.sut.location(at: 0)
            let hongkong = self.sut.location(at: 1)
            XCTAssertEqual(london?.name, "London")
            XCTAssertEqual(hongkong?.name, "HongKong")
        }
    }
    
    func testlocationAtIndex_WhenLocationNotFoundOnPassedIndex_returnsNil() {
        // Arrange
        let asynExpectation = expectation(description: "asyncFunction")
        mockViewDelegate.expectation = asynExpectation
        
        // Act
        sut.loadFavouriteLocations()
        mockWeatherManager.completionHandler?([WMLocation(id: 123, name: "London"), WMLocation(id: 456, name: "HongKong")], nil)
        
        // Assert
        waitForExpectations(timeout: 1.0) { error in
            if let error = error {
                XCTFail("waitForExpectationsWithTimeout errored: \(error)")
            }
            let retLocation = self.sut.location(at: 2)
            XCTAssertNil(retLocation)
        }
    }
}
