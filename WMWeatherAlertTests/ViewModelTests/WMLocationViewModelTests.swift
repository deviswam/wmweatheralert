//
//  WMLocationViewModel.swift
//  WMWeatherAlert
//
//  Created by Waheed Malik on 18/03/2017.
//  Copyright © 2017 Waheed Malik. All rights reserved.
//

import XCTest
@testable import WMWeatherAlert

class WMLocationViewModelTests: XCTestCase {
    func testLocationNameIsTakenFromLocationObject() {
        //Arrange
        let location = WMLocation(id: 123, name: "London")
        let sut = WMLocationViewModel(location: location)
        //Assert
        XCTAssertEqual(sut.name, location.name)
    }
    
    func testCurrentWindSpeedIsTakenFromLocationsCurrentWindConditionObject() {
        //Arrange
        var location = WMLocation(id: 123)
        location.currentWindCondition = WMWindCondition(speed: 19.5)
        let sut = WMLocationViewModel(location: location)
        //Assert
        XCTAssertEqual(sut.currentWindSpeed, "19.5 mph")
    }
    
    func testCurrentWindDirectionIsTakenFromLocationsCurrentWindConditionObject() {
        //Arrange
        var location = WMLocation(id: 123)
        location.currentWindCondition = WMWindCondition(direction: "NW")
        let sut = WMLocationViewModel(location: location)
        //Assert
        XCTAssertEqual(sut.currentWindDirection, "NW")
    }
}
