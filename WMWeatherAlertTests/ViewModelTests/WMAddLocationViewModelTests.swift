//
//  WMAddLocationViewModelTests.swift
//  WMWeatherAlert
//
//  Created by Waheed Malik on 19/03/2017.
//  Copyright © 2017 Waheed Malik. All rights reserved.
//

import XCTest
@testable import WMWeatherAlert

extension WMAddLocationViewModelTests {
    class MockWeatherManager: WeatherManager {
        var searchLocationWithName = ""
        var completionHandler: (([Location]?, WeatherManagerError?) -> Void)?
        
        func searchLocation(with name: String, completionHandler: @escaping ([Location]?, WeatherManagerError?) -> Void) {
            searchLocationWithName = name
            self.completionHandler = completionHandler
        }
        
        func favLocations(completionHandler: @escaping (_ locations: [Location]?, _ error: WeatherManagerError?) -> Void) {
        }
        
        func addFavouriteLocation(location: Location) -> Bool {
            return false
        }
    }
    
    class MockAddLocationViewModelViewDelegate: AddLocationViewModelViewDelegate {
        var expectation: XCTestExpectation?
        var locationsLoadedDelegateMethodCalled = false
        
        func locationsLoaded(viewModel: AddLocationViewModel) {
            guard let asynExpectation = expectation else {
                XCTFail("MockAddLocationViewModelViewDelegate was not setup correctly. Missing XCTExpectation reference")
                return
            }
            locationsLoadedDelegateMethodCalled = true
            asynExpectation.fulfill()
        }
    }
    
    class MockAddLocationViewModelCoordinatorDelegate: AddLocationViewModelCoordinatorDelegate {
        func didAddFavouriteLocation(viewModel: AddLocationViewModel) {
        }
    }
}

class WMAddLocationViewModelTests: XCTestCase {
    
    var sut: WMAddLocationViewModel!
    var mockWeatherManager: MockWeatherManager!
    var mockViewDelegate: MockAddLocationViewModelViewDelegate!
    var mockCoordinatorDelegate: MockAddLocationViewModelCoordinatorDelegate!
    
    override func setUp() {
        super.setUp()
        
        mockWeatherManager = MockWeatherManager()
        mockViewDelegate = MockAddLocationViewModelViewDelegate()
        mockCoordinatorDelegate = MockAddLocationViewModelCoordinatorDelegate()
        sut = WMAddLocationViewModel(weatherManager: mockWeatherManager, viewDelegate: mockViewDelegate, coordinatorDelegate: mockCoordinatorDelegate)
    }
    
    func testSearchLocation_shouldAskWeatherManagerToSearchLocation() {
        // Act
        sut.searchLocation(with: "London")
        // Assert
        XCTAssertEqual(mockWeatherManager.searchLocationWithName, "London")
    }
    
    func testSearchLocation_whenNoLocationsFound_raisesLocationsLoadedDelegateCallback() {
        // Arrange
        let asynExpectation = expectation(description: "asyncFunction")
        mockViewDelegate.expectation = asynExpectation
        
        // Act
        sut.searchLocation(with: "London")
        mockWeatherManager.completionHandler?(nil, nil)
        
        waitForExpectations(timeout: 1.0) {[unowned self] error in
            if let error = error {
                XCTFail("waitForExpectationsWithTimeout errored: \(error)")
            }
            XCTAssert(self.mockViewDelegate.locationsLoadedDelegateMethodCalled)
        }
    }
    
    func testNoOfSearchedLocations_whenNoLocationsFound_returnsZero() {
        // Arrange
        let asynExpectation = expectation(description: "asyncFunction")
        mockViewDelegate.expectation = asynExpectation
        
        // Act
        sut.searchLocation(with: "London")
        mockWeatherManager.completionHandler?(nil, nil)
        
        // Assert
        waitForExpectations(timeout: 1.0) { error in
            if let error = error {
                XCTFail("waitForExpectationsWithTimeout errored: \(error)")
            }
            XCTAssertEqual(self.sut.numberOfSearchedLocations(), 0)
        }
    }

    func testNoOfSearchedLocations_whenOneLocationFound_returnsOne() {
        // Arrange
        let asynExpectation = expectation(description: "asyncFunction")
        mockViewDelegate.expectation = asynExpectation
        
        // Act
        sut.searchLocation(with: "London")
        mockWeatherManager.completionHandler?([WMLocation(id: 123)], nil)
        
        // Assert
        waitForExpectations(timeout: 1.0) { error in
            if let error = error {
                XCTFail("waitForExpectationsWithTimeout errored: \(error)")
            }
            XCTAssertEqual(self.sut.numberOfSearchedLocations(), 1)
        }
    }

    func testNoOfSearchedLocations_whenFiveLocationsFound_returnsFive() {
        // Arrange
        let asynExpectation = expectation(description: "asyncFunction")
        mockViewDelegate.expectation = asynExpectation
        
        // Act
        sut.searchLocation(with: "London")
        mockWeatherManager.completionHandler?([WMLocation(id: 123), WMLocation(id: 456), WMLocation(id: 789), WMLocation(id: 567), WMLocation(id: 876)], nil)
        
        // Assert
        waitForExpectations(timeout: 1.0) { error in
            if let error = error {
                XCTFail("waitForExpectationsWithTimeout errored: \(error)")
            }
            XCTAssertEqual(self.sut.numberOfSearchedLocations(), 5)
        }
    }

    func testlocationAtIndex_returnsTheCorrectLocationViewModelObject() {
        // Arrange
        let asynExpectation = expectation(description: "asyncFunction")
        mockViewDelegate.expectation = asynExpectation
        
        // Act
        sut.searchLocation(with: "London")
        mockWeatherManager.completionHandler?([WMLocation(id: 123, name: "London"), WMLocation(id: 456, name: "HongKong")], nil)
        
        // Assert
        waitForExpectations(timeout: 1.0) { error in
            if let error = error {
                XCTFail("waitForExpectationsWithTimeout errored: \(error)")
            }
            let london = self.sut.location(at: 0)
            let hongkong = self.sut.location(at: 1)
            XCTAssertEqual(london?.name, "London")
            XCTAssertEqual(hongkong?.name, "HongKong")
        }
    }

    func testlocationAtIndex_WhenLocationNotFoundOnPassedIndex_returnsNil() {
        // Arrange
        let asynExpectation = expectation(description: "asyncFunction")
        mockViewDelegate.expectation = asynExpectation
        
        // Act
        sut.searchLocation(with: "London")
        mockWeatherManager.completionHandler?([WMLocation(id: 123, name: "London"), WMLocation(id: 456, name: "HongKong")], nil)
        
        // Assert
        waitForExpectations(timeout: 1.0) { error in
            if let error = error {
                XCTFail("waitForExpectationsWithTimeout errored: \(error)")
            }
            let retLocation = self.sut.location(at: 2)
            XCTAssertNil(retLocation)
        }
    }
}
