//
//  LocationTableViewCellTests.swift
//  WMWeatherAlert
//
//  Created by Waheed Malik on 18/03/2017.
//  Copyright © 2017 Waheed Malik. All rights reserved.
//

import XCTest
@testable import WMWeatherAlert

//MARK: MOCKS & FAKES
extension WMLocationTableViewCellTests {
    class FakeDataSource: NSObject, UITableViewDataSource {
        func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
            return 1
        }
        
        func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
            return WMLocationTableViewCell()
        }
    }
    
    class FakeLocationViewModel: LocationViewModel {
        var name: String { return "San Francisco"}
        var currentWindSpeed: String { return "30 mph"}
        var currentWindDirection: String { return "NWN"}
    }
}

class WMLocationTableViewCellTests: XCTestCase {
    let fakeDataSource = FakeDataSource()
    var tableView : UITableView!
    var cell : WMLocationTableViewCell!
    
    override func setUp() {
        super.setUp()
        let storyBoard = UIStoryboard.init(name: "Main", bundle: Bundle.main)
        let favLocationsVC = storyBoard.instantiateViewController(withIdentifier: "WMFavLocationsVC") as! WMFavLocationsVC
        favLocationsVC.locationsTableViewDataSource = fakeDataSource
        UIApplication.shared.keyWindow?.rootViewController = favLocationsVC
        _ = favLocationsVC.view
        tableView = favLocationsVC.locationsTableView
        cell = tableView.dequeueReusableCell(withIdentifier: "LocationTableViewCell", for: IndexPath(row: 0, section: 0)) as! WMLocationTableViewCell
    }
    
    func testLocationCell_HasNameLabel() {
        // Assert
        XCTAssertNotNil(cell.locationNameLabel)
    }
    
    func testLocationCell_HasWindSpeedLabel() {
        // Assert
        XCTAssertNotNil(cell.windSpeedLabel)
    }
    
    func testLocationCell_HasWindDirectionLabel() {
        // Assert
        XCTAssertNotNil(cell.windDirectionLabel)
    }
    
    func testConfigCell_SetNameWindSpeedAndWindDirectionOnLabels() {
        //Act
        cell.configCell(withLocation: FakeLocationViewModel())
        
        //Assert
        XCTAssertEqual(cell.locationNameLabel.text!, "San Francisco")
        XCTAssertEqual(cell.windSpeedLabel.text!, "30 mph")
        XCTAssertEqual(cell.windDirectionLabel.text!, "NWN")
    }
}
