//
//  LocationsTableViewDataSourceTests.swift
//  WMWeatherAlert
//
//  Created by Waheed Malik on 18/03/2017.
//  Copyright © 2017 Waheed Malik. All rights reserved.
//

import XCTest
@testable import WMWeatherAlert

extension WMLocationsTableViewDataSourceTests {
    class MockTableView: UITableView {
        var cellGotDequeued = false
        override func dequeueReusableCell(withIdentifier identifier: String, for indexPath: IndexPath) -> UITableViewCell {
            cellGotDequeued = true
            return super.dequeueReusableCell(withIdentifier: identifier, for: indexPath)
        }
    }
    
    class MockLocationTableViewCell: UITableViewCell, LocationTableViewCell {
        var configCellGotCalled = false
        var location : LocationViewModel?
        func configCell(withLocation location: LocationViewModel) {
            configCellGotCalled = true
            self.location = location
        }
    }
    
    class MockFavLocationsViewModel: FavLocationsViewModel {
        var locations: [Location]?
        func numberOfFavouriteLocations() -> Int {
            if let locations = self.locations { return locations.count }
            return 0
        }
        
        func location(at index: Int) -> LocationViewModel? {
            if let locations = locations, index < locations.count {
                return WMLocationViewModel(location: locations[index])
            }
            return nil
        }
        
        func loadFavouriteLocations() { }
        func showAddFavouriteLocation() { }
    }
}

class WMLocationsTableViewDataSourceTests: XCTestCase {
    func testDataSourceHasFavLocationsViewModel() {
        // Arrange
        let sut = WMLocationsTableViewDataSource()
        sut.viewModel = MockFavLocationsViewModel()
        // Assert
        XCTAssertNotNil(sut.viewModel)
        XCTAssertNotNil((sut.viewModel! as Any) is FavLocationsViewModel)
    }
    
    func testNumberOfRowsInTheSection_WithTwoLocations_ShouldReturnTwo() {
        //Arrange
        let sut = WMLocationsTableViewDataSource()
        let locations = [WMLocation(id: 123), WMLocation(id: 456)]
        let mockFavLocationsViewModel = MockFavLocationsViewModel()
        mockFavLocationsViewModel.locations = locations
        sut.viewModel = mockFavLocationsViewModel
        let mockTableView = MockTableView()
        
        //Act
        let noOfItemsInSectionZero = sut.tableView(mockTableView, numberOfRowsInSection: 0)
        
        //Assert
        XCTAssertEqual(noOfItemsInSectionZero, 2, "Number of items in section are 2")
    }
    
    func testCellForRow_ReturnsLocationTableViewCell() {
        //Arrange
        let sut = WMLocationsTableViewDataSource()
        let locations = [WMLocation(id: 123), WMLocation(id: 456)]
        let mockFavLocationsViewModel = MockFavLocationsViewModel()
        mockFavLocationsViewModel.locations = locations
        sut.viewModel = mockFavLocationsViewModel
        let mockTableView = MockTableView(frame: CGRect.zero)
        mockTableView.dataSource = sut
        mockTableView.register(MockLocationTableViewCell.self, forCellReuseIdentifier: "LocationTableViewCell")
        
        //Act
        let cell = sut.tableView(mockTableView, cellForRowAt: IndexPath(item: 0, section: 0))
        
        //Assert
        XCTAssertTrue(cell is LocationTableViewCell,"Returned cell is of LocationTableViewCell type")
    }
    
    func testCellForRow_DequeuesCell() {
        //Arrange
        let sut = WMLocationsTableViewDataSource()
        let locations = [WMLocation(id: 123), WMLocation(id: 456)]
        let mockFavLocationsViewModel = MockFavLocationsViewModel()
        mockFavLocationsViewModel.locations = locations
        sut.viewModel = mockFavLocationsViewModel
        
        let mockTableView = MockTableView(frame: CGRect.zero)
        mockTableView.dataSource = sut
        mockTableView.register(MockLocationTableViewCell.self, forCellReuseIdentifier: "LocationTableViewCell")
        
        //Act
        _ = sut.tableView(mockTableView, cellForRowAt: IndexPath(item: 0, section: 0))
        
        //Assert
        XCTAssertTrue(mockTableView.cellGotDequeued,"CellForRow should be calling DequeueCell method")
    }
    
    func testConfigCell_GetsCalledFromCellForRow() {
        //Arrange
        let sut = WMLocationsTableViewDataSource()
        let expectedLocation = WMLocation(id: 123, name: "London")
        let locations = [expectedLocation]
        let mockFavLocationsViewModel = MockFavLocationsViewModel()
        mockFavLocationsViewModel.locations = locations
        sut.viewModel = mockFavLocationsViewModel
        
        let mockTableView = MockTableView(frame: CGRect.zero)
        mockTableView.dataSource = sut
        mockTableView.register(MockLocationTableViewCell.self, forCellReuseIdentifier: "LocationTableViewCell")
        
        //Act
        let cell = sut.tableView(mockTableView, cellForRowAt: IndexPath(item: 0, section: 0)) as! MockLocationTableViewCell
        
        //Assert
        XCTAssertTrue(cell.configCellGotCalled,"CellForRow should be calling ConfigCell method")
        XCTAssertTrue(cell.location?.name  == expectedLocation.name, "Location should be same")
    }
}
