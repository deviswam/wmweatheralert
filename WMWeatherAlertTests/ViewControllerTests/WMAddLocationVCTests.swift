//
//  WMAddLocationVCTests.swift
//  WMWeatherAlert
//
//  Created by Waheed Malik on 19/03/2017.
//  Copyright © 2017 Waheed Malik. All rights reserved.
//

import XCTest
@testable import WMWeatherAlert

extension WMAddLocationVCTests {
    class MockAddLocationViewModel: AddLocationViewModel {
        var searchLocationCalled = false
        var searchLocationName = ""
        func searchLocation(with name: String) {
            searchLocationCalled = true
            searchLocationName = name
        }
        
        func numberOfSearchedLocations() -> Int {
            return 0
        }
        
        func location(at index: Int) -> LocationViewModel? {
            return nil
        }
        
        func selectedLocation(at index: Int) {
        }
    }
    
    class MockAddLocationSearchBarDelegate: WMAddLocationSearchBarDelegate {
        override func searchBarSearchButtonClicked(_ searchBar: UISearchBar) {
            viewModel?.searchLocation(with: searchBar.text!)
        }
    }
}

class WMAddLocationVCTests: XCTestCase {
    
    var sut: WMAddLocationVC!
    
    override func setUp() {
        super.setUp()
        
        // Arrange
        let storyBoard = UIStoryboard(name: "Main", bundle: Bundle.main)
        sut = storyBoard.instantiateViewController(withIdentifier: "WMAddLocationVC") as! WMAddLocationVC
    }
    
    func testLocationSearchBar_whenViewIsLoaded_hasDelegate() {
        // Arrange
        sut.locationSearchBarDelegate = WMAddLocationSearchBarDelegate()
        // Act
        _ = sut.view
        // Assert
        XCTAssert(sut.locationSearchBar.delegate is WMAddLocationSearchBarDelegate)
    }
    
    func testLocationsTableView_whenViewIsLoaded_isNotNil() {
        //Act
        _ = sut.view
        // Assert
        XCTAssertNotNil(sut.locationsTableView)
    }
    
    func testLocationsTableView_whenViewIsLoaded_hasDataSource() {
        // Act
        sut.addLocationTableViewDataSource = WMAddLocationTableViewDataSource()
        _ = sut.view
        // Assert
        XCTAssert(sut.locationsTableView.dataSource is WMAddLocationTableViewDataSource)
    }
    
    func testLocationsTableView_whenViewIsLoaded_hasDelegate() {
        // Arrange
        sut.addLocationTableViewDelegate = WMAddLocationTableViewDelegate()
        // Act
        _ = sut.view
        // Assert
        XCTAssert(sut.locationsTableView.delegate is WMAddLocationTableViewDelegate)
    }
    
    func testSearchLocation_whenSearchButtonIsPressed_shouldAskViewModelForLocationSearch() {
        // Arrange
        let mockViewModel = MockAddLocationViewModel()
        let mockAddLocationSearchBarDelegate = MockAddLocationSearchBarDelegate()
        mockAddLocationSearchBarDelegate.viewModel = mockViewModel
        let fakeSearchBar = UISearchBar()
        fakeSearchBar.text = "London"
        
        // Act
        mockAddLocationSearchBarDelegate.searchBarSearchButtonClicked(fakeSearchBar)
    
        // Assert
        XCTAssert(mockViewModel.searchLocationCalled)
        XCTAssertEqual(mockViewModel.searchLocationName, "London")
    }
}
