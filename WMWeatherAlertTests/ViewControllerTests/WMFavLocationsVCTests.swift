//
//  FavLocationsVCTests.swift
//  WMWeatherAlert
//
//  Created by Waheed Malik on 18/03/2017.
//  Copyright © 2017 Waheed Malik. All rights reserved.
//

import XCTest
@testable import WMWeatherAlert

extension WMFavLocationsVCTests {
    class MockFavLocationsViewModel: FavLocationsViewModel {
        func numberOfFavouriteLocations() -> Int { return 0 }
        func location(at index: Int) -> LocationViewModel? { return nil }
        func showAddFavouriteLocation() { }
        
        var loadFavLocationsCalled = false
        func loadFavouriteLocations() {
            self.loadFavLocationsCalled = true
        }
    }
}

class WMFavLocationsVCTests: XCTestCase {
    
    var sut: WMFavLocationsVC!
    
    override func setUp() {
        super.setUp()
        
        // Arrange
        let storyBoard = UIStoryboard(name: "Main", bundle: Bundle.main)
        sut = storyBoard.instantiateViewController(withIdentifier: "WMFavLocationsVC") as! WMFavLocationsVC
    }
    
    func testLocationsTableView_whenViewIsLoaded_isNotNil() {
        //Act
        _ = sut.view
        // Assert
        XCTAssertNotNil(sut.locationsTableView)
    }
    
    func testLocationsTableView_whenViewIsLoaded_hasDataSource() {
        // Act
        sut.locationsTableViewDataSource = WMLocationsTableViewDataSource()
        _ = sut.view
        // Assert
        XCTAssert(sut.locationsTableView.dataSource is WMLocationsTableViewDataSource)
    }
    
    func testLocationsTableView_whenViewIsLoaded_hasDelegate() {
        // Arrange
        sut.locationsTableViewDelegate = WMLocationsTableViewDelegate()
        // Act
        _ = sut.view
        // Assert
        XCTAssert(sut.locationsTableView.delegate is WMLocationsTableViewDelegate)
    }
    
    func testLoadFavLocations_onViewWillAppear_shouldAskViewModelForFavLocations() {
        // Arrange
        let mockViewModel = MockFavLocationsViewModel()
        sut.viewModel = mockViewModel
        //Act
        sut.viewWillAppear(true)
        // Assert
        XCTAssert(mockViewModel.loadFavLocationsCalled)
    }
}
