//
//  WMWeatherAPI.swift
//  WMWeatherAlert
//
//  Created by Waheed Malik on 18/03/2017.
//  Copyright © 2017 Waheed Malik. All rights reserved.
//

import XCTest
import Foundation
@testable import WMWeatherAlert

extension WMWeatherAPITests {
    class MockURLSession: URLSession {
        typealias Handler = (Data?, URLResponse?, Error?) -> Void
        
        var dataTaskRequestMethodCalled = false
        var urlRequest : URLRequest?
        var completionHandler : Handler?
        var dataTask = MockURLSessionDataTask()
        
        override func dataTask(with urlRequest: URLRequest, completionHandler: @escaping (Data?, URLResponse?, Error?) -> Void) -> URLSessionDataTask {
            dataTaskRequestMethodCalled = true
            self.urlRequest = urlRequest
            self.completionHandler = completionHandler
            return dataTask
        }
    }
    
    class MockURLSessionDataTask : URLSessionDataTask {
        var resumeGotCalled = false
        override func resume() {
            resumeGotCalled = true
        }
    }
}

class WMWeatherAPITests: XCTestCase {
    var mockURLSession: MockURLSession!
    var sut: WMWeatherAPI!
    
    override func setUp() {
        super.setUp()
        mockURLSession = MockURLSession()
        sut = WMWeatherAPI(urlSession: mockURLSession)
    }
    
    func testConformanceToWeatherAPIProtocol() {
        XCTAssertTrue((sut as Any) is WeatherAPI)
    }
    
    func testCurrentWindCondition_ShouldAskURLSessionForFetchingCurrentWeatherData() {
        // Act
        sut.currentWindCondition(for: [123,456]) { (locations, error) in  }
        
        // Assert
        XCTAssert(mockURLSession.dataTaskRequestMethodCalled)
    }
    
    func testCurrentWindCondition_WithValidJSON_ShouldReturnALocationWithIdAndName() {
        // Arrange
        let asynExpectation = expectation(description: "AsynFunction")
        
        let currentWeatherAPIResponseInString = "{\"list\":[{" +
            "\"id\":524901," +
            "\"name\":\"Moscow\"" +
        "}]}"
        
        let responseData = currentWeatherAPIResponseInString.data(using: .utf8)
        let expectedLocationId = 524901
        var receivedLocation : Location?
        var receivedError : WeatherAPIError?
        
        //Act
        sut.currentWindCondition(for: [expectedLocationId]) { (locations: [Location]?, error: WeatherAPIError?) in
            if let locations = locations {
                receivedLocation = locations[0] 
            }
            receivedError = error
            asynExpectation.fulfill()
            
        }
        mockURLSession.completionHandler!(responseData, nil, nil)
        
        waitForExpectations(timeout: 1.0) { error in
            //Assert
            XCTAssertTrue(receivedLocation?.id == expectedLocationId, "Fetched and expected location should have same id")
            XCTAssertTrue(receivedLocation?.name == "Moscow", "Fetched and expected location should have same name")
            XCTAssertNil(receivedError, "Nil should be returned as an Error along with Location object")
        }
    }
    
    func testCurrentWindCondition_WithValidJSON_ShouldReturnMultipleLocationsWithRespectiveIdsAndNames() {
        // Arrange
        let asynExpectation = expectation(description: "AsynFunction")
        
        let currentWeatherAPIResponseInString = "{\"list\":[{" +
            "\"id\":524901," +
            "\"name\":\"Moscow\"" +
            "},{" +
            "\"id\":703448," +
            "\"name\":\"London\"" +
            "}" +
            "]}"
        
        let responseData = currentWeatherAPIResponseInString.data(using: .utf8)
        let expectedLocations = [WMLocation(id: 524901, name: "Moscow"),WMLocation(id: 703448, name: "London")]
        var receivedLocations : [Location]?
        var receivedError : WeatherAPIError?
        
        //Act
        sut.currentWindCondition(for: [expectedLocations[0].id, expectedLocations[1].id]) { (locations: [Location]?, error: WeatherAPIError?) in
            if let locations = locations {
                receivedLocations = locations
            }
            receivedError = error
            asynExpectation.fulfill()
            
        }
        mockURLSession.completionHandler!(responseData, nil, nil)
        
        waitForExpectations(timeout: 1.0) { error in
            //Assert
            XCTAssert(receivedLocations?.count == 2)
            XCTAssertTrue(receivedLocations?[0].id == expectedLocations[0].id, "Fetched and expected location should have same id")
            XCTAssertTrue(receivedLocations?[1].id == expectedLocations[1].id, "Fetched and expected location should have same id")
            XCTAssertTrue(receivedLocations?[0].name == expectedLocations[0].name, "Fetched and expected location should have same name")
            XCTAssertTrue(receivedLocations?[1].name == expectedLocations[1].name, "Fetched and expected location should have same name")
            XCTAssertNil(receivedError, "Nil should be returned as an Error along with Locations object")
        }
    }
    
    func testCurrentWindCondition_WithValidJSON_ShouldReturnALocationWithItsCurrentWindCondition() {
        // Arrange
        let asynExpectation = expectation(description: "AsynFunction")
        
        let currentWeatherAPIResponseInString = "{\"list\":[{" +
            "\"wind\":{" +
            "\"speed\":4," +
            "\"deg\":270" +
            "}," +
            "\"dt\":1489748400," +
            "\"id\":524901," +
            "\"name\":\"Moscow\"" +
        "}]}"
        
        let responseData = currentWeatherAPIResponseInString.data(using: .utf8)
        let expectedDate = Date(timeIntervalSince1970: 1489748400)
        var receivedLocation : Location?
        var receivedError : WeatherAPIError?
        
        //Act
        sut.currentWindCondition(for: [524901]) { (locations: [Location]?, error: WeatherAPIError?) in
            if let locations = locations {
                receivedLocation = locations[0]
            }
            receivedError = error
            asynExpectation.fulfill()
            
        }
        mockURLSession.completionHandler!(responseData, nil, nil)
        
        waitForExpectations(timeout: 1.0) { error in
            //Assert
            XCTAssertEqual(receivedLocation?.currentWindCondition?.speed, 4, "Fetched and expected wind condition should have same speed")
            XCTAssertEqual(receivedLocation?.currentWindCondition?.direction, "W", "Fetched and expected wind condition should have same direction")
            XCTAssertEqual(receivedLocation?.currentWindCondition?.date, expectedDate, "Fetched and expected wind condition should have same date")
            XCTAssertNil(receivedError, "Nil should be returned as an Error")
        }
    }
    
    func testCurrentWindCondition_WithValidJSON_ShouldReturnMultipleLocationsWithTheirCurrentWindConditions() {
        // Arrange
        let asynExpectation = expectation(description: "AsynFunction")
        
        let currentWeatherAPIResponseInString = "{\"list\":[" +
            "{\"wind\":{" +
            "\"speed\":4," +
            "\"deg\":220" +
            "}," +
            "\"dt\":1489611600," +
            "\"id\":524901," +
            "\"name\":\"Moscow\"" +
            "}," +
            "{\"wind\":{" +
            "\"speed\":5," +
            "\"deg\":300" +
            "}," +
            "\"dt\":1489748400," +
            "\"id\":703448," +
            "\"name\":\"London\"" +
            "}" +
        "]}"
        
        let responseData = currentWeatherAPIResponseInString.data(using: .utf8)
        
        let expectedDateWindCondition1 = Date(timeIntervalSince1970: 1489611600)
        let expectedWindCondition1 = WMWindCondition(speed: 4, direction: "SW", date: expectedDateWindCondition1)
        
        let expectedDateWindCondition2 = Date(timeIntervalSince1970: 1489748400)
        let expectedWindCondition2 = WMWindCondition(speed: 5, direction: "WNW", date: expectedDateWindCondition2)
        
        let expectedLocations = [WMLocation(id: 524901, name: "Moscow", currentWindCondition: expectedWindCondition1),
                                 WMLocation(id: 703448, name: "London", currentWindCondition: expectedWindCondition2)]
        var receivedLocations : [Location]?
        var receivedError : WeatherAPIError?
        
        //Act
        sut.currentWindCondition(for: [expectedLocations[0].id, expectedLocations[1].id]) { (locations: [Location]?, error: WeatherAPIError?) in
            if let locations = locations {
                receivedLocations = locations
            }
            receivedError = error
            asynExpectation.fulfill()
            
        }
        mockURLSession.completionHandler!(responseData, nil, nil)
        
        waitForExpectations(timeout: 1.0) { error in
            //Assert
            XCTAssert(receivedLocations?.count == 2)
            
            XCTAssertEqual(receivedLocations?[0].currentWindCondition?.speed, expectedWindCondition1.speed, "Fetched and expected wind condition should have same speed")
            XCTAssertEqual(receivedLocations?[0].currentWindCondition?.direction, expectedWindCondition1.direction, "Fetched and expected wind condition should have same direction")
            XCTAssertEqual(receivedLocations?[0].currentWindCondition?.date, expectedWindCondition1.date, "Fetched and expected wind condition should have same date")
            
            XCTAssertEqual(receivedLocations?[1].currentWindCondition?.speed, expectedWindCondition2.speed, "Fetched and expected wind condition should have same speed")
            XCTAssertEqual(receivedLocations?[1].currentWindCondition?.direction, expectedWindCondition2.direction, "Fetched and expected wind condition should have same direction")
            XCTAssertEqual(receivedLocations?[1].currentWindCondition?.date, expectedWindCondition2.date, "Fetched and expected wind condition should have same date")
            
            XCTAssertNil(receivedError, "Nil should be returned as an Error")
        }
    }
}
