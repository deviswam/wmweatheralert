//
//  WMWeatherManagerTests.swift
//  WMWeatherAlert
//
//  Created by Waheed Malik on 18/03/2017.
//  Copyright © 2017 Waheed Malik. All rights reserved.
//

import XCTest
@testable import WMWeatherAlert

extension WMWeatherManagerTests {
    class MockUserDefaultsManager: UserDefaultsManager {
        var favLocationIdsCalled = false
        var completionHandler: (([Int]?) -> Void)?
        func favLocationIds(completionHandler: @escaping ([Int]?) -> Void) {
            favLocationIdsCalled = true
            self.completionHandler = completionHandler
        }
        func addFavLocation(id: Int) -> Bool {
            return false
        }
    }
    
    class MockWeatherAPI: WeatherAPI {
        var currentWindConditionCalled = false
        var completionHandler: ((_ locations: [Location]?, _ error: WeatherAPIError?) -> Void)?
        func currentWindCondition(for locationIds: [Int], completionHandler: @escaping (_ locations: [Location]?, _ error: WeatherAPIError?) -> Void) {
            self.completionHandler = completionHandler
            currentWindConditionCalled = true
        }
        
        var findCitiesCalled = false
        func findCities(with matchingName: String, completionHandler: @escaping ([Location]?, WeatherAPIError?) -> Void) {
            self.completionHandler = completionHandler
            findCitiesCalled = true
        }
    }
}

class WMWeatherManagerTests: XCTestCase {
    
    var mockUserDefaultsManager: MockUserDefaultsManager!
    var mockWeatherAPI: MockWeatherAPI!
    var sut: WMWeatherManager!
    
    override func setUp() {
        super.setUp()
        mockUserDefaultsManager = MockUserDefaultsManager()
        mockWeatherAPI = MockWeatherAPI()
        sut = WMWeatherManager(userDefaultsManager: mockUserDefaultsManager, weatherAPI: mockWeatherAPI)
    }
    
    func testConformanceToWeatherManagerProtocol() {
        //Assert
        XCTAssertTrue((sut as Any) is WeatherManager, "WMWeatherManager should conforms to WeatherManager protocol")
    }
    
    func testFavLocations_shouldAskUserDefaultsManagerForStoredLocationIds() {
        // Act
        sut.favLocations { (locations, error) in
            
        }
        // Assert
        XCTAssert(mockUserDefaultsManager.favLocationIdsCalled)
    }
    
    func testFavLocations_whenNoStoredLocationsFound_returnsNoProductsFoundError() {
        // Arrange
        var receivedLocations : [Location]?
        var receivedError : WeatherManagerError?
        
        // Act
        sut.favLocations { (locations, error) in
            receivedLocations = locations
            receivedError = error
        }
        mockUserDefaultsManager.completionHandler?(nil)
        
        // Assert
        XCTAssertEqual(receivedError, WeatherManagerError.noLocationsFoundError)
        XCTAssertNil(receivedLocations)
    }
    
    func testFavLocations_whenStoredLocationsFound_asksWeatherAPIToFetchTheirCurrentWindCondition() {
        // Act
        sut.favLocations { (locations, error) in

        }
        mockUserDefaultsManager.completionHandler?([123, 456])
        
        // Assert
        XCTAssert(mockWeatherAPI.currentWindConditionCalled)
    }
    
    func testFavLocations_shouldFetchLocationsDataWithNoErrorForAllStoredLocations() {
        // Arrange
        var receivedLocations : [Location]?
        var receivedError : WeatherManagerError?
        let expectedLocations = [WMLocation(id: 123), WMLocation(id: 456)]
        
        // Act
        sut.favLocations { (locations, error) in
            receivedLocations = locations
            receivedError = error
        }
        
        mockUserDefaultsManager.completionHandler?([123, 456])
        mockWeatherAPI.completionHandler?(expectedLocations, nil)
        
        
        // Assert
        XCTAssert(receivedLocations![0] == expectedLocations[0])
        XCTAssert(receivedLocations![1] == expectedLocations[1])
        XCTAssertNil(receivedError)
    }
    
    func testSearchLocation_withLocationName_asksWeatherAPIToSearchForMatchingCities() {
        // Act
        sut.searchLocation(with: "London") { (locations, error) in
            
        }
        
        // Assert
        XCTAssert(mockWeatherAPI.findCitiesCalled)
    }
    
    func testSearchLocation_shouldFetchLocationsDataWithNoErrorForAllMatchingLocations() {
        // Arrange
        var receivedLocations : [Location]?
        var receivedError : WeatherManagerError?
        let expectedLocations = [WMLocation(id: 123, name:"London,UK"), WMLocation(id: 456, name:"London,US")]
        
        // Act
        sut.searchLocation(with: "London") { (locations, error) in
            receivedLocations = locations
            receivedError = error
        }
        mockWeatherAPI.completionHandler?(expectedLocations, nil)
        
        
        // Assert
        XCTAssert(receivedLocations![0] == expectedLocations[0])
        XCTAssert(receivedLocations![1] == expectedLocations[1])
        XCTAssertNil(receivedError)
    }
}
