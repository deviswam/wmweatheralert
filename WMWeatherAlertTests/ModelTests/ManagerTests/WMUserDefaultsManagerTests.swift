//
//  WMUserDefaultsManagerTests.swift
//  WMWeatherAlert
//
//  Created by Waheed Malik on 18/01/2017.
//  Copyright © 2017 Waheed Malik. All rights reserved.
//

import XCTest
@testable import WMWeatherAlert

class MockUserDefaults: UserDefaults {
    var getArrayForKeyCalled = false
    var retDictionary: [String: [Any]]?
    override func array(forKey defaultName: String) -> [Any]? {
        getArrayForKeyCalled = true
        return retDictionary?[defaultName]
    }
}

class WMUserDefaultsManagerTests: XCTestCase {
    
    func testfavLocationIds_shouldAskUserDefaultsForStoredLocationIds() {
        // Arrange
        let mockUserDefaults = MockUserDefaults()
        let sut = WMUserDefaultsManager(userDefaults: mockUserDefaults)
        // Act
        sut.favLocationIds(completionHandler: { (locationIds) in
            
        })
        // Assert
        XCTAssert(mockUserDefaults.getArrayForKeyCalled)
    }
    
    func testFavLocationIds_whenNoStoredLocationIdFound_returnsNil() {
        // Arrange
        let mockUserDefaults = MockUserDefaults()
        let sut = WMUserDefaultsManager(userDefaults: mockUserDefaults)
        // Act
        var retLocations: [Int]?
        sut.favLocationIds(completionHandler: { (locationIds) in
            retLocations = locationIds
        })
        // Assert
        XCTAssertNil(retLocations)
    }

    func testFavLocationIds_whenStoredLocationIdsFound_returnIds() {
        // Arrange
        let mockUserDefaults = MockUserDefaults()
        let sut = WMUserDefaultsManager(userDefaults: mockUserDefaults)
        mockUserDefaults.retDictionary = [sut.kFAV_LOCATION_IDS: [123, 456]]
        // Act
        var retLocations: [Int]!
        sut.favLocationIds(completionHandler: { (locationIds) in
            retLocations = locationIds
        })
        // Assert
        XCTAssertEqual(123, retLocations[0])
        XCTAssertEqual(456, retLocations[1])
    }
}
