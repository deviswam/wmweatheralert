//
//  WMWindConditionTests.swift
//  WMWeatherAlert
//
//  Created by Waheed Malik on 18/03/2017.
//  Copyright © 2017 Waheed Malik. All rights reserved.
//

import XCTest
@testable import WMWeatherAlert

class WMWindConditionTests: XCTestCase {
    func testConformanceToWindConditionProtocol() {
        // Arrange
        let sut = WMWindCondition()
        // Assert
        XCTAssert((sut as Any) is WindCondition)
    }
    
    func testInit_WindConditionShouldHaveSpeed() {
        //Arrange
        let sut = WMWindCondition(speed: 19.5)
        
        //Assert
        XCTAssertEqual(sut.speed, 19.5, "Wind condition should have a speed")
    }
    
    func testInit_WindConditionShouldHaveDirection() {
        //Arrange
        let sut = WMWindCondition(direction: "NW")
        
        //Assert
        XCTAssertEqual(sut.direction, "NW", "Wind condition should have a direction")
    }
    
    func testInit_WindConditionShouldHaveDate() {
        //Arrange
        let currentDateTime = Date()
        let sut = WMWindCondition(date: currentDateTime)
        
        //Assert
        XCTAssertEqual(sut.date, currentDateTime, "Wind condition should have a date")
    }
    
    func testWindConditionConformsToObjectMapperProtocol() {
        //Arrange
        let sut = WMWindCondition()
        
        //Assert
        XCTAssertTrue((sut as Any) is ObjectMapper, "WindCondition should conforms to ObjectMapper protocol")
    }

    func testInit_WithNilDictionary_ShouldFailInitialization() {
        //Arrange
        let sut = WMWindCondition(dictionary: nil)
        
        //Assert
        XCTAssertNil(sut, "WindCondition should be created only with valid json dictionary")
    }

    func testInit_WithWindSpeedAndDirectionAndDateInDictionary_ShouldSetAllFieldsWithCorrectTransformation() {
        //Arrange
        let expectedDate = Date(timeIntervalSince1970: 1489748400)
        
        //Act
        let jsonWindDictionary = ["speed":19.5, "deg":270, "dt":1489748400]
        let sut = WMWindCondition(dictionary: jsonWindDictionary)
        
        //Assert
        XCTAssertEqual(sut?.speed, 19.5, "speed should be same")
        XCTAssertEqual(sut?.direction, "W", "270 degrees should be W cardinal direction")
        XCTAssertEqual(sut?.date, expectedDate, "returned and expected dates should be same")
    }
}
