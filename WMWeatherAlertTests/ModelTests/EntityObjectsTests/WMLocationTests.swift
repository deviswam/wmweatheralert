//
//  LocationTests.swift
//  WMWeatherAlert
//
//  Created by Waheed Malik on 18/03/2017.
//  Copyright © 2017 Waheed Malik. All rights reserved.
//

import Foundation

import XCTest
@testable import WMWeatherAlert

class WMLocationTests: XCTestCase {
    func testConformanceToLocationProtocol() {
        // Arrange
        let sut = WMLocation(id: 0)
        // Assert
        XCTAssert((sut as Any) is Location)
    }
    
    func testInit_LocationMustBeCreatedWithAnId() {
        // Arrange
        let locationId = 123
        let sut = WMLocation(id: locationId)
        // Assert
        XCTAssertEqual(sut.id, locationId)
    }
    
    func testLocationShouldHaveAName() {
        // Arrange
        let locationId = 123
        let name = "London"
        let sut = WMLocation(id: locationId, name: name)
        // Assert
        XCTAssertEqual(sut.name, name)
    }
    
    func testLocationsForEquality_WithSameId_AreSameLocations() {
        //Arrange
        let location1 = WMLocation(id: 123, name: "London")
        let location2 = WMLocation(id: 123)
        
        //Assert
        XCTAssert(location1 == location2, "Both are same locations")
    }
    
    func testLocationConformsToObjectMapperProtocol() {
        //Arrange
        let location = WMLocation(id: 123)
        
        //Assert
        XCTAssertTrue((location as Any) is ObjectMapper, "Location should conforms to ObjectMapper protocol")
    }
    
    func testInit_WithNilDictionary_ShouldFailInitialization() {
        //Arrange
        let location = WMLocation(dictionary: nil)
        
        //Assert
        XCTAssertNil(location, "Location should be created only with valid json dictionary")
    }
    
    func testInit_WithNoLocationIdInDictionary_ShouldFailInitialization() {
        //Arrange
        let jsonLocationDictionary = ["name":"London"]
        let location = WMLocation(dictionary: jsonLocationDictionary as Dictionary<String, AnyObject>?)
        //Assert
        XCTAssertNil(location, "Location should only be created if dictionary has location Id")
    }
    
    func testInit_WithNoLocationNameInDictionary_ShouldNotFailInitialization() {
        //Arrange
        let jsonLocationDictionary = ["id":123]
        let location = WMLocation(dictionary: jsonLocationDictionary as Dictionary<String, AnyObject>?)
        
        //Assert
        XCTAssertNotNil(location, "Only Location id is mandatory to create Location object")
    }
    
    func testInit_WithLocationIdAndNameInDictionary_ShouldSetLocationIdAndName() {
        //Arrange
        let jsonLocationDictionary = ["id":123, "name":"London"] as [String : Any]
        let location = WMLocation(dictionary: jsonLocationDictionary as Dictionary<String, AnyObject>?)
        
        //Assert
        XCTAssertEqual(location?.id, 123)
        XCTAssertEqual(location?.name, "London")
    }
    
    func testInit_WithWindConditionInDictionary_ShouldSetCurrentWindConditionOfLocation() {
        //Arrange
        let date: TimeInterval = 1489748400
        let expectedDate = Date(timeIntervalSince1970: date)
        let jsonWindDictionary = ["speed":19.5, "deg":270]
        let jsonLocationDictionary = ["id":123,"wind":jsonWindDictionary,"dt":date] as [String : Any]
        let location = WMLocation(dictionary: jsonLocationDictionary)
        //Assert
        XCTAssertNotNil(location?.currentWindCondition)
        XCTAssertEqual(location?.currentWindCondition?.speed, 19.5)
        XCTAssertEqual(location?.currentWindCondition?.direction, "W")
        XCTAssertEqual(location?.currentWindCondition?.date, expectedDate)
    }
}
